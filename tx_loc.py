#! /usr/bin/python3

import itertools
from joblib import Parallel, delayed
import numpy as np
try:
    import cupy as cp
except ImportError:
    cp = np
import matplotlib.pyplot as plt
import functools as ft
from scipy.io import wavfile
import sys

# set up transmitters
samp_rate = 44100
tx = {'loc': np.array([.75, .25]), 'sig': np.array([1, -1, 1, -1])}
tx_l = [tx]
tx = {'loc': np.array([.55, .60]), 'sig': np.array([1, 1, 1, 1])}
tx_l.append(tx)
tx = {'loc': np.array([.3, .3]), 'sig': np.array([1, 1, 1, 1], dtype=float)}
tx_l.append(tx)
tx = {'loc': np.array([.5, 1.75]), 'sig': np.array([1, -1, -1, 1])}
tx_l.append(tx)

wav_files = sys.argv[1:]
print("wav_files = ", wav_files)
for [n, f_name] in enumerate(wav_files):
    fs, sig = wavfile.read(f_name)
    tx_l[n]['sig'] = sig[:]/(1.0*len(wav_files))
    print(f_name, tx_l[n]['sig'].size, fs)

# set up receivers
rx_l = [{'loc': np.array([0.0, 0.0]), 'sig':np.array([])},
        {'loc': np.array([1.0, 0.0]), 'sig':np.array([])},
        {'loc': np.array([1.0, 1.0]), 'sig':np.array([])},
        {'loc': np.array([0.0, 1.0]), 'sig':np.array([])}]

# Calculate delays
c = 331
for tx in tx_l:
    for rx in rx_l:
        n = int(samp_rate*np.linalg.norm(tx['loc'] - rx['loc'])/c)
        sig = np.concatenate([np.zeros(n), tx['sig']])
        if sig.size > rx['sig'].size:
            rx['sig'].resize(sig.size)
        else:
            sig.resize(rx['sig'].size)
        rx['sig'] += sig


# create Correlation matrix
x_c = .5
y_c = .5
ranges = 1.0
spacial_sampling = 100.0/ranges
mat_len = int(np.ceil(ranges * spacial_sampling))
mat = np.zeros([mat_len, mat_len])


def cross_ambiguity(row, col):
    x = row/spacial_sampling + (x_c - ranges/2)
    y = col/spacial_sampling + (y_c - ranges/2)
    pixel_loc = np.array([x, y])
    sig_list = []
    for rx in rx_l:
        n = int(samp_rate*np.linalg.norm(pixel_loc - rx['loc'])/c)
        sig = rx['sig'][n:-1]
        sig_list.append(sig)
    max_sig_len = np.max([el.size for el in sig_list])
    sig_list_resized = []
    for sig in sig_list:
        gpu_array = cp.asarray(np.concatenate(
            [sig, np.zeros(max_sig_len-sig.size)]))
        sig_list_resized.append(gpu_array)
    # assign intensity
    return np.power(np.abs(np.sum(
        ft.reduce(lambda x, y: x*y, sig_list_resized))),
        1.0/len(sig_list_resized))


results = Parallel(n_jobs=-1)(delayed(cross_ambiguity)(row, col) for row, col
                              in itertools.product(range(mat.shape[0]),
                                                   range(mat.shape[1])))
mat = np.reshape(results, mat.shape)

plt.imshow(mat,
           cmap='hot',
           interpolation='nearest',
           extent=[x_c-ranges/2, x_c+ranges/2, y_c-ranges/2, y_c+ranges/2],
           origin='lower')
ax = plt.gca()
plt.show()
