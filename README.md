# TX Location

Locates emitters based on time difference of arrival using correlation.

## Usage

`tx_loc <up to 4 wav files>`

## Examples

Below are several simulated examples with 4 listeners at (0, 0), (0, 1), (1, 1), and (1, 0).

Demo simple signals at (.75, .25), (.55, .6), and (.3, .3):

![](images/demo.png)

Audio of a clap at (.75, .25)

![](images/clap.png)

Audio of a clap at (.75, .25), and noise at (.55, .60)

![](images/clap_noise.png)
